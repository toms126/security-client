import requests
import argparse
import warnings
warnings.simplefilter("ignore")

def post_http():
    data = {
        "username": "test@test.com.au",
        "password": "1729a3c4-7a5e-4258-a921-9f74601d3981"
    }
    requests.post("http://192.168.1.11:5000/oauth/login", data)

def post_https():
    data = {
        "username": "test@test.com.au",
        "password": "1729a3c4-7a5e-4258-a921-9f74601d3981"
    }
    requests.post("https://192.168.1.10:5000/oauth/login", data, verify=False)


parser = argparse.ArgumentParser("Http Request Tool")
parser.add_argument("-https", "--https", default="false", help="True / False: If true requests is sent over https")
parser.parse_args()
args = parser.parse_args()
print(args.https)
if (args.https.lower() == "true"):
    print("Sending HTTPS request")
    post_https()
    print("Request Sent")
else:
    print("Sending HTTP request")
    post_http()
    print("Request Sent")